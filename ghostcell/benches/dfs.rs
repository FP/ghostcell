use criterion::{black_box, criterion_group, criterion_main, Criterion};
use typed_arena::Arena as TypedArena;

static EDGE_SIZE : usize = 4;

mod ghost {
    use crate::EDGE_SIZE;
    use ghostcell::GhostToken;
    use ghostcell::dfs_arena::*;
    use typed_arena::Arena as TypedArena;
    use rayon::prelude::*;

    pub fn init<'arena, 'id>(
        arena : &'arena TypedArena<Node<'arena, 'id, u32>>,
        token : &mut GhostToken<'id>,
        node_size : usize,
    ) -> (NodeRef<'arena, 'id, u32>, Graph<'arena, 'id, u32>) {
        let mut graph = Graph::new(node_size);
        for i in 0..node_size as u32 {
            graph.add_node_with_capacity(i, EDGE_SIZE, arena);
        }

        let nodes = graph.nodes();
        // For the first half of the nodes, link every node to the next (EDGE_SIZE - 1) nodes
        for i in 0..(node_size/2) {
            for j in 1..EDGE_SIZE {
                nodes[i].borrow_mut(token).add_edge(nodes[i+j]);
            }
        }
        // For the second half of the nodes, link every node to the previous (EDGE_SIZE - 1) nodes
        for i in (node_size/2)..(node_size) {
            for j in 1..EDGE_SIZE {
                nodes[i].borrow_mut(token).add_edge(nodes[i-j]);
            }
        }

        // Also link the first half to the second half
        for i in 0..(node_size/2) {
            nodes[i].borrow_mut(token).add_edge(nodes[i+node_size/2]);
        }
        for i in (node_size/2+1)..(node_size) {
            nodes[i].borrow_mut(token).add_edge(nodes[i-node_size/2]);
        }

        (nodes[0], graph)
    }

    pub fn dfs_immut<'arena, 'id>(
        graph: &Graph<'arena, 'id, u32>,
        root: NodeRef<'arena, 'id, u32>,
        token: &GhostToken<'id>,
        f: impl Fn(&u32),
    ) {
        let mut dfs = graph.dfs_visitor(root);
        for n in dfs.iter(token) {
            f(n);
        }
    }

    pub fn dfs_mut<'arena, 'id>(
        graph: &Graph<'arena, 'id, u32>,
        root: NodeRef<'arena, 'id, u32>,
        token: &mut GhostToken<'id>,
        f: impl FnMut(&mut u32)
    ) {
        let mut dfs = graph.dfs_visitor(root);
        dfs.iter_mut(token, f);
    }

    pub fn par<'arena, 'id>(
        num_threads: usize,
        graph: &Graph<'arena, 'id, u32>,
        root: NodeRef<'arena, 'id, u32>,
        token: &GhostToken<'id>,
        f: impl Fn(&u32) + Sync,
    ) {
        (0..num_threads)
            .into_par_iter()
            .for_each(|_| dfs_immut(graph,root, token, |n| f(n)));
    }

    pub fn par_inner(f: impl Fn(&u32) + Sync) {
        rayon::join(|| f(&0), || f(&0));
    }
}

mod ghost_list {
    use crate::EDGE_SIZE;
    use ghostcell::GhostToken;
    use ghostcell::dfs_arena_list::*;
    use ghostcell::list_arena::Node as SNode;
    use typed_arena::Arena as TypedArena;
    use rayon::prelude::*;

    pub fn init<'arena, 'id>(
        arena : &'arena TypedArena<Node<'arena, 'id, u32>>,
        larena : &'arena TypedArena<SNode<'arena, 'id, NodeRef<'arena, 'id, u32>>>,
        token : &mut GhostToken<'id>,
        node_size : usize,
    ) -> (NodeRef<'arena, 'id, u32>, Graph<'arena, 'id, u32>) {
        let mut graph = Graph::new(node_size);
        for i in 0..node_size as u32 {
            graph.add_node(i, arena);
        }

        let nodes = graph.nodes();
        // For the first half of the nodes, link every node to the next (EDGE_SIZE - 1) nodes
        for i in 0..(node_size/2) {
            for j in 1..EDGE_SIZE {
                Node::add_edge(nodes[i], nodes[i+j], token, larena);
            }
        }
        // For the second half of the nodes, link every node to the previous (EDGE_SIZE - 1) nodes
        for i in (node_size/2)..(node_size) {
            for j in 1..EDGE_SIZE {
                Node::add_edge(nodes[i], nodes[i-j], token, larena);
            }
        }

        // Also link the first half to the second half
        for i in 0..(node_size/2) {
            Node::add_edge(nodes[i], nodes[i+node_size/2], token, larena);
        }
        for i in (node_size/2+1)..(node_size) {
            Node::add_edge(nodes[i], nodes[i-node_size/2], token, larena);
        }

        (nodes[0], graph)
    }

    pub fn dfs_immut<'arena, 'id>(
        graph: &Graph<'arena, 'id, u32>,
        root: NodeRef<'arena, 'id, u32>,
        token: &GhostToken<'id>,
        f: impl Fn(&u32),
    ) {
        let mut dfs = graph.dfs_visitor(root);
        for n in dfs.iter(token) {
            f(n);
        }
    }

    pub fn dfs_mut<'arena, 'id>(
        graph: &Graph<'arena, 'id, u32>,
        root: NodeRef<'arena, 'id, u32>,
        token: &mut GhostToken<'id>,
        f: impl FnMut(&mut u32)
    ) {
        let mut dfs = graph.dfs_visitor(root);
        dfs.iter_mut(token, f);
    }

    pub fn par<'arena, 'id>(
        num_threads: usize,
        graph: &Graph<'arena, 'id, u32>,
        root: NodeRef<'arena, 'id, u32>,
        token: &GhostToken<'id>,
        f: impl Fn(&u32) + Sync,
    ) {
        (0..num_threads)
            .into_par_iter()
            .for_each(|_| dfs_immut(graph, root, token, |n| f(n)));
    }
}

mod rwlock {
    use crate::EDGE_SIZE;
    use typed_arena::Arena as TypedArena;
    use std::sync::RwLock;
    use fixedbitset::FixedBitSet;
    use rayon::prelude::*;

    struct NodeBase<'arena, T> {
        data : T,
        edges : Vec<NodeRef<'arena, T>>,
    }
    /// A graph node.
    pub struct Node<'arena, T> {
        uid : u32,
        inner : RwLock<NodeBase<'arena, T>>
    }

    /// A reference to a node.
    pub type NodeRef<'arena, T> = &'arena Node<'arena, T>;

    impl<'arena, T> Node<'arena, T> {
        /// Create a new isolated node from T. Requires an arena.
        fn new(
            data : T,
            uid : u32,
            arena : &'arena TypedArena<Node<'arena, T>>
        ) -> NodeRef<'arena, T> {
            arena.alloc(
                Self {
                    uid: uid,
                    inner: RwLock::new(
                        NodeBase {
                            data: data,
                            edges: vec![]
                        }),
                }
            )
        }

        fn with_capacity(
            data: T,
            uid : u32,
            edge_size : usize,
            arena : &'arena TypedArena<Node<'arena, T>>
        ) -> NodeRef<'arena, T> {
            arena.alloc(
                Self {
                    uid: uid,
                    inner: RwLock::new(
                        NodeBase {
                            data: data,
                            edges: Vec::with_capacity(edge_size),
                        }),
                }
            )
        }

        pub fn uid(&self) -> u32 {
            self.uid
        }

        pub fn add_edge(&self, child : NodeRef<'arena, T>) {
            self.inner.write().unwrap().edges.push(child)
        }
    }

    /// A graph with a root. Internally has a vector to keep track of all nodes in the graph.
    pub struct Graph<'arena, T> {
        nodes : Vec<NodeRef<'arena, T>>,
    }

    impl<'arena, T> Graph<'arena, T> {
        pub fn new(node_size : usize) -> Self {
            Self {
                nodes: Vec::with_capacity(node_size),
            }
        }

        pub fn add_node(
            &mut self,
            data : T,
            arena : &'arena TypedArena<Node<'arena, T>>
        ) -> NodeRef<'arena, T> {
            let node = Node::new(data, self.nodes.len() as u32, arena);
            self.nodes.push(node);
            node
        }

        pub fn add_node_with_capacity(
            &mut self,
            data: T,
            edge_size : usize,
            arena : &'arena TypedArena<Node<'arena, T>>
        ) -> NodeRef<'arena, T> {
            let node = Node::with_capacity(data, self.nodes.len() as u32, edge_size, arena);
            self.nodes.push(node);
            node
        }

        pub fn nodes_count(&self) -> usize {
            self.nodes.len()
        }

        pub fn nodes(&self) -> &Vec<NodeRef<'arena, T>> {
            &self.nodes
        }

        pub fn dfs_iter(
            & self,
            root: NodeRef<'arena, T>
        ) -> DFSIter<'arena, T> {
            DFSIter {
                stack: vec![root],
                mark: FixedBitSet::with_capacity(self.nodes.len()),
            }
        }
    }

    /// A DFS traversal structure that stores a visit map.
    pub struct DFSIter<'arena, T>{
        stack: Vec<NodeRef<'arena, T>>,
        mark: FixedBitSet,
    }

    impl<'arena, T> DFSIter<'arena, T> {
        pub fn reset(
            &mut self,
            graph: &Graph<'arena, T>,
            root: NodeRef<'arena, T>
        ) {
            self.stack.clear();
            self.stack.push(root);
            self.mark.clear();
            self.mark.grow(graph.nodes_count());
        }

        fn visit(&mut self, n: u32) -> bool {
            self.mark.put(n as usize)
        }

        fn is_visited(&self, n: u32) -> bool {
            self.mark.contains(n as usize)
        }

        pub fn iter(
            &mut self,
            f: impl Fn(&T),
        ) {
            while let Some(node) = self.stack.pop() {
                let uid = node.uid;
                let node = node.inner.read().unwrap();
                if !self.is_visited(uid) {
                    self.visit(uid);
                    f(&node.data);
                    for child in node.edges.iter() {
                        if !self.is_visited(child.uid) {
                            self.stack.push(child)
                        }
                    }
                }
            }
        }

        pub fn iter_mut(
            &mut self,
            mut f: impl FnMut(&mut T),
        ) {
            while let Some(node) = self.stack.pop() {
                let uid = node.uid;
                let mut node = node.inner.write().unwrap();
                if !self.is_visited(uid) {
                    self.visit(uid);
                    f(&mut node.data);
                    for child in node.edges.iter() {
                        if !self.is_visited(child.uid) {
                            self.stack.push(child)
                        }
                    }
                }
            }
        }
    }


    pub fn init<'arena>(
        arena : &'arena TypedArena<Node<'arena, u32>>,
        node_size : usize,
    ) -> (NodeRef<'arena, u32>, Graph<'arena, u32>) {
        let mut graph = Graph::new(node_size);
        for i in 0..node_size as u32 {
            graph.add_node_with_capacity(i, EDGE_SIZE, arena);
        }

        let nodes = graph.nodes();
        // For the first half of the nodes, link every node to the next (EDGE_SIZE - 1) nodes
        for i in 0..(node_size/2) {
            for j in 1..EDGE_SIZE {
                nodes[i].add_edge(nodes[i+j]);
            }
        }
        // For the second half of the nodes, link every node to the previous (EDGE_SIZE - 1) nodes
        for i in (node_size/2)..(node_size) {
            for j in 1..EDGE_SIZE {
                nodes[i].add_edge(nodes[i-j]);
            }
        }

        // Also link the first half to the second half
        for i in 0..(node_size/2) {
            nodes[i].add_edge(nodes[i+node_size/2]);
        }
        for i in (node_size/2+1)..(node_size) {
            nodes[i].add_edge(nodes[i-node_size/2]);
        }
        (nodes[0], graph)
    }

    pub fn dfs_immut<'arena>(
        graph: &Graph<'arena, u32>,
        root: NodeRef<'arena, u32>,
        f: impl Fn(&u32),
    ) {
        let mut dfs = graph.dfs_iter(root);
        dfs.iter(f);
    }

    pub fn dfs_mut<'arena>(
        graph: &Graph<'arena, u32>,
        root: NodeRef<'arena, u32>,
        f: impl FnMut(&mut u32)
    ) {
        let mut dfs = graph.dfs_iter(root);
        dfs.iter_mut(f);
    }

    pub fn par<'arena>(
        num_threads: usize,
        graph: &Graph<'arena, u32>,
        root: NodeRef<'arena, u32>,
        f: impl Fn(&u32) + Sync,
    ) {
        (0..num_threads)
            .into_par_iter()
            .for_each(|_| dfs_immut(graph, root, |n| f(n)));
    }
}

mod mutex {
    use crate::EDGE_SIZE;
    use typed_arena::Arena as TypedArena;
    use std::sync::Mutex;
    use fixedbitset::FixedBitSet;
    use rayon::prelude::*;

    struct NodeBase<'arena, T> {
        data : T,
        edges : Vec<NodeRef<'arena, T>>,
    }
    /// A graph node.
    pub struct Node<'arena, T> {
        uid : u32,
        inner : Mutex<NodeBase<'arena, T>>
    }

    /// A reference to a node.
    pub type NodeRef<'arena, T> = &'arena Node<'arena, T>;

    impl<'arena, T> Node<'arena, T> {
        /// Create a new isolated node from T. Requires an arena.
        fn new(
            data : T,
            uid : u32,
            arena : &'arena TypedArena<Node<'arena, T>>
        ) -> NodeRef<'arena, T> {
            arena.alloc(
                Self {
                    uid: uid,
                    inner: Mutex::new(
                        NodeBase {
                            data: data,
                            edges: vec![]
                        }),
                }
            )
        }

        fn with_capacity(
            data: T,
            uid : u32,
            edge_size : usize,
            arena : &'arena TypedArena<Node<'arena, T>>
        ) -> NodeRef<'arena, T> {
            arena.alloc(
                Self {
                    uid: uid,
                    inner: Mutex::new(
                        NodeBase {
                            data: data,
                            edges: Vec::with_capacity(edge_size),
                        }),
                }
            )
        }

        pub fn uid(&self) -> u32 {
            self.uid
        }

        pub fn add_edge(&self, child : NodeRef<'arena, T>) {
            self.inner.lock().unwrap().edges.push(child)
        }
    }

    /// A graph with a root. Internally has a vector to keep track of all nodes in the graph.
    pub struct Graph<'arena, T> {
        nodes : Vec<NodeRef<'arena, T>>,
    }

    impl<'arena, T> Graph<'arena, T> {
        pub fn new(node_size : usize) -> Self {
            Self {
                nodes: Vec::with_capacity(node_size),
            }
        }

        pub fn add_node(
            &mut self,
            data : T,
            arena : &'arena TypedArena<Node<'arena, T>>
        ) -> NodeRef<'arena, T> {
            let node = Node::new(data, self.nodes.len() as u32, arena);
            self.nodes.push(node);
            node
        }

        pub fn add_node_with_capacity(
            &mut self,
            data: T,
            edge_size : usize,
            arena : &'arena TypedArena<Node<'arena, T>>
        ) -> NodeRef<'arena, T> {
            let node = Node::with_capacity(data, self.nodes.len() as u32, edge_size, arena);
            self.nodes.push(node);
            node
        }

        pub fn nodes_count(&self) -> usize {
            self.nodes.len()
        }

        pub fn nodes(&self) -> &Vec<NodeRef<'arena, T>> {
            &self.nodes
        }

        pub fn dfs_iter(
            & self,
            root: NodeRef<'arena, T>
        ) -> DFSIter<'arena, T> {
            DFSIter {
                stack: vec![root],
                mark: FixedBitSet::with_capacity(self.nodes.len()),
            }
        }
    }

    /// A DFS traversal structure that stores a visit map.
    pub struct DFSIter<'arena, T>{
        stack: Vec<NodeRef<'arena, T>>,
        mark: FixedBitSet,
    }

    impl<'arena, T> DFSIter<'arena, T> {
        pub fn reset(
            &mut self,
            graph: &Graph<'arena, T>,
            root: NodeRef<'arena, T>
        ) {
            self.stack.clear();
            self.stack.push(root);
            self.mark.clear();
            self.mark.grow(graph.nodes_count());
        }

        fn visit(&mut self, n: u32) -> bool {
            self.mark.put(n as usize)
        }

        fn is_visited(&self, n: u32) -> bool {
            self.mark.contains(n as usize)
        }

        pub fn iter(
            &mut self,
            f: impl Fn(&T),
        ) {
            while let Some(node) = self.stack.pop() {
                let uid = node.uid;
                let node = node.inner.lock().unwrap();
                if !self.is_visited(uid) {
                    self.visit(uid);
                    f(&node.data);
                    for child in node.edges.iter() {
                        if !self.is_visited(child.uid) {
                            self.stack.push(child)
                        }
                    }
                }
            }
        }

        pub fn iter_mut(
            &mut self,
            mut f: impl FnMut(&mut T),
        ) {
            while let Some(node) = self.stack.pop() {
                let uid = node.uid;
                let mut node = node.inner.lock().unwrap();
                if !self.is_visited(uid) {
                    self.visit(uid);
                    f(&mut node.data);
                    for child in node.edges.iter() {
                        if !self.is_visited(child.uid) {
                            self.stack.push(child)
                        }
                    }
                }
            }
        }
    }


    pub fn init<'arena>(
        arena : &'arena TypedArena<Node<'arena, u32>>,
        node_size : usize,
    ) -> (NodeRef<'arena, u32>, Graph<'arena, u32>) {
        let mut graph = Graph::new(node_size);
        for i in 0..node_size as u32 {
            graph.add_node_with_capacity(i, EDGE_SIZE, arena);
        }

        let nodes = graph.nodes();
        // For the first half of the nodes, link every node to the next (EDGE_SIZE - 1) nodes
        for i in 0..(node_size/2) {
            for j in 1..EDGE_SIZE {
                nodes[i].add_edge(nodes[i+j]);
            }
        }
        // For the second half of the nodes, link every node to the previous (EDGE_SIZE - 1) nodes
        for i in (node_size/2)..(node_size) {
            for j in 1..EDGE_SIZE {
                nodes[i].add_edge(nodes[i-j]);
            }
        }

        // Also link the first half to the second half
        for i in 0..(node_size/2) {
            nodes[i].add_edge(nodes[i+node_size/2]);
        }
        for i in (node_size/2+1)..(node_size) {
            nodes[i].add_edge(nodes[i-node_size/2]);
        }
        (nodes[0], graph)
    }

    pub fn dfs_immut<'arena>(
        graph: &Graph<'arena, u32>,
        root: NodeRef<'arena, u32>,
        f: impl Fn(&u32),
    ) {
        let mut dfs = graph.dfs_iter(root);
        dfs.iter(f);
    }

    pub fn dfs_mut<'arena>(
        graph: &Graph<'arena, u32>,
        root: NodeRef<'arena, u32>,
        f: impl FnMut(&mut u32)
    ) {
        let mut dfs = graph.dfs_iter(root);
        dfs.iter_mut(f);
    }

    pub fn par<'arena>(
        num_threads: usize,
        graph: &Graph<'arena, u32>,
        root: NodeRef<'arena, u32>,
        f: impl Fn(&u32) + Sync,
    ) {
        (0..num_threads)
            .into_par_iter()
            .for_each(|_| dfs_immut(graph, root, |n| f(n)));
    }
}

mod pet {
    use crate::EDGE_SIZE;
    use petgraph::graph::Graph;
    use petgraph::graph::NodeIndex;
    use petgraph::visit::Dfs;
    use rayon::prelude::*;

    pub fn init(node_size : usize) -> (Graph<u32, ()>, NodeIndex) {
        let mut graph : Graph<u32, ()> = Graph::with_capacity(node_size, node_size * EDGE_SIZE);
        let mut nodes : Vec<NodeIndex> = Vec::with_capacity(node_size);

        for i in 0..node_size as u32 {
            nodes.push(graph.add_node(i))
        }
        // For the first half of the nodes, link every node to the next (EDGE_SIZE - 1) nodes
        for i in 0..(node_size/2) {
            for j in 1..EDGE_SIZE {
                graph.add_edge(nodes[i], nodes[i+j], ());
            }
        }
        // For the second half of the nodes, link every node to the previous (EDGE_SIZE - 1) nodes
        for i in (node_size/2)..(node_size) {
            for j in 1..EDGE_SIZE {
                graph.add_edge(nodes[i], nodes[i-j], ());
            }
        }

        // Also link the first half to the second half
        for i in 0..(node_size/2) {
            graph.add_edge(nodes[i], nodes[i+node_size/2], ());
        }
        for i in (node_size/2+1)..(node_size) {
            graph.add_edge(nodes[i], nodes[i-node_size/2], ());
        }

        (graph, nodes[0])
    }

    pub fn dfs_immut(
        graph: &Graph<u32, ()>,
        root : NodeIndex,
        f: impl Fn(&u32),
    ) {
        let mut dfs = Dfs::new(graph, root);
        while let Some(visited) = dfs.next(graph) {
            f(& graph[visited])
        }
    }

    pub fn dfs_mut(
        graph: &mut Graph<u32, ()>,
        root : NodeIndex,
        mut f: impl FnMut(&mut u32),
    ) {
        let mut dfs = Dfs::new(&*graph, root);
        while let Some(visited) = dfs.next(&*graph) {
            f(&mut graph[visited])
        }
    }

    pub fn par(
        num_threads: usize,
        graph: &Graph<u32, ()>,
        root : NodeIndex,
        f: impl Fn(&u32) + Sync,
    ) {
        (0..num_threads)
            .into_par_iter()
            .for_each(|_| dfs_immut(graph, root, |n| f(n)));
    }
}

use ghostcell::GhostToken;
use std::time::{Instant, Duration};

fn bench_dfs(c: &mut Criterion) {
    let node_size = 50_000;

    c.bench_function("rayon overhead", |b| {
        b.iter(|| {
            ghost::par_inner(|n| {
                black_box(n);
            });
        });
    });

    c.bench_function("dfs arena ghostcell vec init", |b| {
        GhostToken::new(|mut token| {
            b.iter_custom(|iters| {
                let mut elapsed = Duration::new(0,0);
                for _i in 0..iters {
                    let start = Instant::now();
                    let arena = TypedArena::with_capacity(black_box(node_size as usize));
                    black_box(ghost::init(&arena, &mut token, black_box(node_size)));
                    elapsed += start.elapsed();
                }
                elapsed
            });
        });
    });
    c.bench_function("dfs arena ghostcell vec init no_drop", |b| {
        GhostToken::new(|mut token| {
            b.iter_custom(|iters| {
                let mut elapsed = Duration::new(0,0);
                for _i in 0..iters {
                    let start = Instant::now();
                    let arena = TypedArena::with_capacity(black_box(node_size as usize));
                    let mut r = black_box(ghost::init(&arena, &mut token, black_box(node_size)));
                    black_box(&mut r);
                    elapsed += start.elapsed();
                    drop(r);
                }
                elapsed
            });
        });
    });
    c.bench_function("dfs arena ghostcell vec immut", |b| {
        GhostToken::new(|mut token| {
            let arena = TypedArena::with_capacity(node_size as usize);
            let (root, graph) = ghost::init(&arena, &mut token, node_size);
            b.iter(|| {
                black_box(ghost::dfs_immut(&graph, root, &token, |n| {
                    black_box(n);
                }));
            });
        });
    });
    c.bench_function("dfs arena ghostcell vec par immut (×4)", |b| {
        GhostToken::new(|mut token| {
            let arena = TypedArena::with_capacity(node_size as usize);
            let (root, graph) = ghost::init(&arena, &mut token, node_size);
            b.iter(|| {
                black_box(ghost::par(4, &graph, root, &token, |n| {
                    black_box(n);
                }));
            });
        });
    });
    c.bench_function("dfs arena ghostcell vec mut", |b| {
        GhostToken::new(|mut token| {
            let arena = TypedArena::with_capacity(node_size as usize);
            let (root, graph) = ghost::init(&arena, &mut token, node_size);
            b.iter(|| {
                black_box(ghost::dfs_mut(&graph, root, &mut token, |n| {
                    black_box(n);
                }));
            });
        });
    });

    c.bench_function("dfs arena ghostcell list init", |b| {
        GhostToken::new(|mut token| {
            b.iter_custom(|iters| {
                let mut elapsed = Duration::new(0,0);
                for _i in 0..iters {
                    let start = Instant::now();
                    let arena = TypedArena::with_capacity(black_box(node_size as usize));
                    let larena = TypedArena::with_capacity(black_box(node_size * EDGE_SIZE as usize));
                    black_box(ghost_list::init(&arena, &larena, &mut token, black_box(node_size)));
                    elapsed += start.elapsed();
                }
                elapsed
            });
        });
    });
    c.bench_function("dfs arena ghostcell list init no_drop", |b| {
        GhostToken::new(|mut token| {
            b.iter_custom(|iters| {
                let mut elapsed = Duration::new(0,0);
                for _i in 0..iters {
                    let start = Instant::now();
                    let arena = TypedArena::with_capacity(black_box(node_size as usize));
                    let larena = TypedArena::with_capacity(black_box(node_size * EDGE_SIZE as usize));
                    let mut r = black_box(ghost_list::init(&arena, &larena, &mut token, black_box(node_size)));
                    black_box(&mut r);
                    elapsed += start.elapsed();
                    drop(r);
                }
                elapsed
            });
        });
    });
    c.bench_function("dfs arena ghostcell list immut", |b| {
        GhostToken::new(|mut token| {
            let arena = TypedArena::with_capacity(node_size as usize);
            let larena = TypedArena::with_capacity(black_box(node_size * EDGE_SIZE as usize));
            let (root, graph) = ghost_list::init(&arena, &larena, &mut token, node_size);
            b.iter(|| {
                black_box(ghost_list::dfs_immut(&graph, root, &token, |n| {
                    black_box(n);
                }));
            });
        });
    });
    c.bench_function("dfs arena ghostcell list par immut (×4)", |b| {
        GhostToken::new(|mut token| {
            let arena = TypedArena::with_capacity(node_size as usize);
            let larena = TypedArena::with_capacity(black_box(node_size * EDGE_SIZE as usize));
            let (root, graph) = ghost_list::init(&arena, &larena, &mut token, node_size);
            b.iter(|| {
                black_box(ghost_list::par(4, &graph, root, &token, |n| {
                    black_box(n);
                }));
            });
        });
    });
    c.bench_function("dfs arena ghostcell list mut", |b| {
        GhostToken::new(|mut token| {
            let arena = TypedArena::with_capacity(node_size as usize);
            let larena = TypedArena::with_capacity(black_box(node_size * EDGE_SIZE as usize));
            let (root, graph) = ghost_list::init(&arena, &larena, &mut token, node_size);
            b.iter(|| {
                black_box(ghost_list::dfs_mut(&graph, root, &mut token, |n| {
                    black_box(n);
                }));
            });
        });
    });


    c.bench_function("dfs petgraph init", |b| {
        // We use the same measuring method with other init functions to be fair.
        b.iter_custom(|iters| {
            let mut elapsed = Duration::new(0,0);
            for _i in 0..iters {
                let start = Instant::now();
                black_box(pet::init(black_box(node_size)));
                elapsed += start.elapsed();
            }
            elapsed
        });
    });
    c.bench_function("dfs petgraph init no_drop", |b| {
        // We use the same measuring method with other init functions to be fair.
        b.iter_custom(|iters| {
            let mut elapsed = Duration::new(0,0);
            for _i in 0..iters {
                let start = Instant::now();
                let mut r = black_box(pet::init(black_box(node_size)));
                black_box(&mut r);
                elapsed += start.elapsed();
                drop(r);
            }
            elapsed
        });
    });
    c.bench_function("dfs petgraph immut", |b| {
        let (graph, root) = pet::init(node_size);
        b.iter(|| {
            black_box(pet::dfs_immut(&graph, root, |n| {
                black_box(n);
            }));
        });
    });
    c.bench_function("dfs petgraph par immut (×4)", |b| {
        let (graph, root) = pet::init(node_size);
        b.iter(|| {
            black_box(pet::par(4, &graph, root,  |n| {
                black_box(n);
            }));
        });
    });
    c.bench_function("dfs petgraph mut", |b| {
        let (mut graph, root) = pet::init(node_size);
        b.iter(|| {
            black_box(pet::dfs_mut(&mut graph, root, |n| {
                black_box(n);
            }));
        });
    });


    c.bench_function("dfs arena rwlock init", |b| {
        b.iter_custom(|iters| {
            let mut elapsed = Duration::new(0,0);
            for _i in 0..iters {
                let start = Instant::now();
                let arena = TypedArena::with_capacity(black_box(node_size as usize));
                black_box(rwlock::init(&arena, black_box(node_size)));
                elapsed += start.elapsed();
            }
            elapsed
        });
    });
    c.bench_function("dfs arena rwlock init no_drop", |b| {
        b.iter_custom(|iters| {
            let mut elapsed = Duration::new(0,0);
            for _i in 0..iters {
                let start = Instant::now();
                let arena = TypedArena::with_capacity(black_box(node_size as usize));
                let mut r = black_box(rwlock::init(&arena, black_box(node_size)));
                black_box(&mut r);
                elapsed += start.elapsed();
                drop(r);
            }
            elapsed
        });
    });
    c.bench_function("dfs arena rwlock immut", |b| {
        let arena = TypedArena::with_capacity(node_size as usize);
        let (root, graph) = rwlock::init(&arena, node_size);
        b.iter(|| {
            black_box(rwlock::dfs_immut(&graph, root, |n| {
                black_box(n);
            }));
        });
    });
    c.bench_function("dfs arena rwlock par immut (×4)", |b| {
        let arena = TypedArena::with_capacity(node_size as usize);
        let (root, graph) = rwlock::init(&arena, node_size);
        b.iter(|| {
            black_box(rwlock::par(4, &graph, root, |n| {
                black_box(n);
            }));
        });
    });
    c.bench_function("dfs arena rwlock mut", |b| {
        let arena = TypedArena::with_capacity(node_size as usize);
        let (root, graph) = rwlock::init(&arena, node_size);
        b.iter(|| {
            black_box(rwlock::dfs_mut(&graph, root, |n| {
                black_box(n);
            }));
        });
    });


    c.bench_function("dfs arena mutex init", |b| {
        b.iter_custom(|iters| {
            let mut elapsed = Duration::new(0,0);
            for _i in 0..iters {
                let start = Instant::now();
                let arena = TypedArena::with_capacity(black_box(node_size as usize));
                black_box(mutex::init(&arena, black_box(node_size)));
                elapsed += start.elapsed();
            }
            elapsed
        });
    });
    c.bench_function("dfs arena mutex init no_drop", |b| {
        b.iter_custom(|iters| {
            let mut elapsed = Duration::new(0,0);
            for _i in 0..iters {
                let start = Instant::now();
                let arena = TypedArena::with_capacity(black_box(node_size as usize));
                let mut r = black_box(mutex::init(&arena, black_box(node_size)));
                black_box(&mut r);
                elapsed += start.elapsed();
                drop(r);
            }
            elapsed
        });
    });
    c.bench_function("dfs arena mutex immut", |b| {
        let arena = TypedArena::with_capacity(node_size as usize);
        let (root, graph) = mutex::init(&arena, node_size);
        b.iter(|| {
            black_box(mutex::dfs_immut(&graph, root, |n| {
                black_box(n);
            }));
        });
    });
    c.bench_function("dfs arena mutex par immut (×4)", |b| {
        let arena = TypedArena::with_capacity(node_size as usize);
        let (root, graph) = mutex::init(&arena, node_size);
        b.iter(|| {
            black_box(mutex::par(4, &graph, root, |n| {
                black_box(n);
            }));
        });
    });
    c.bench_function("dfs arena mutex mut", |b| {
        let arena = TypedArena::with_capacity(node_size as usize);
        let (root, graph) = mutex::init(&arena, node_size);
        b.iter(|| {
            black_box(mutex::dfs_mut(&graph, root, |n| {
                black_box(n);
            }));
        });
    });
}

criterion_group!(benches, bench_dfs);
criterion_main!(benches);
