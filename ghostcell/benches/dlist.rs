use criterion::{black_box, criterion_group, criterion_main, Criterion};
use typed_arena::Arena as TypedArena;

mod atomic_cell {
    use crossbeam::atomic::AtomicCell;
    use rayon::prelude::*;
    use typed_arena::Arena as TypedArena;

    pub struct Node<'arena, T> {
        data: T,
        prev: AtomicCell<Option<NodeRef<'arena, T>>>,
        next: AtomicCell<Option<NodeRef<'arena, T>>>,
    }
    pub type NodeRef<'arena, T> = &'arena Node<'arena, T>;

    impl<'arena, T> Node<'arena, T> {
        pub fn new(
            data: T,
            arena: &'arena TypedArena<Node<'arena, T>>,
        ) -> NodeRef<'arena, T> {
            arena.alloc(
                Self {
                    data,
                    prev: AtomicCell::new(None),
                    next: AtomicCell::new(None),
                }
            )
        }
    
        /// Unlink the nodes adjacent to the provided node (but do not touch the node itself).
        fn unlink(node: NodeRef<'arena, T>) {
            let old_prev = node.prev.load();
            let old_next = node.next.load();
            if let Some(next) = old_next {
                next.prev.store(old_prev);
            }
            if let Some(prev) = old_prev {
                prev.next.store(old_next);
            }
        }
    
        pub fn insert_next(
            node: NodeRef<'arena, T>,
            next: NodeRef<'arena, T>,
        ) {
            // Step 1: unlink the prev and next pointers nodes adjacent to next.
            Self::unlink(next);
    
            // Step 2: link node.next pointer and node.next.prev pointer to next.
            let old_node_next = node.next.load();
            if let Some(node_next) = old_node_next {
                node_next.prev.store(Some(next));
            }
            node.next.store(Some(next));
    
            // Step 3: link next to node and old_node_next (old node.next).
            next.prev.store(Some(node));
            next.next.store(old_node_next);
        }
    
        pub fn iterate(
            node: NodeRef<'arena, T>,
            f: impl Fn(&T),
        ) {
            let mut cur: Option<NodeRef<'arena, T>> = Some(node);
            while let Some(node) = cur {
                f(&node.data);
                cur = node.next.load();
            }
        }
    }

    /// Create a dlist segment.
    pub fn init<'arena>(
        arena: &'arena TypedArena<Node<'arena, u32>>,
        list_size: u32,
    ) -> NodeRef<'arena, u32> {
        let list: NodeRef<'arena, u32> = &*Node::new(0, &arena);
        let mut tail = list;

        (1..list_size).for_each(|i| {
            let node = Node::new(i, &arena);
            Node::insert_next(tail, node);
            tail = node;
        });

        list
    }

    pub fn par<'arena, T: Send + Sync>(
        num_threads: usize,
        list: NodeRef<'arena, T>,
        print: impl Fn(&T) + Sync,
    ) {
        (0..num_threads)
            .into_par_iter()
            .for_each(|_| Node::iterate(list, |n| print(n)));
    }
}

mod arena {
    use rayon::prelude::*;
    use ghostcell::GhostToken;
    use ghostcell::dlist_arena::*;
    use typed_arena::Arena as TypedArena;

    /// Create a dlist segment.
    pub fn init<'arena, 'id>(
        arena: &'arena TypedArena<Node<'arena, 'id, u32>>,
        token: &mut GhostToken<'id>,
        list_size: u32,
    ) -> NodeRef<'arena, 'id, u32> {

        let list: NodeRef<'arena, 'id, u32> = &*Node::new(0, &arena);
        let mut tail = list;

        (1..list_size).for_each(|i| {
            let node = Node::new(i, &arena);
            Node::insert_next(tail, node, token);
            tail = node;
        });
        
        list
    }

    pub fn par<'arena, 'id, T: Send + Sync>(
        num_threads: usize,
        list: NodeRef<'arena, 'id, T>,
        token: &GhostToken<'id>,
        print: impl Fn(&T) + Sync,
    ) {
        (0..num_threads)
            .into_par_iter()
            .for_each(|_| Node::iterate(list, token, |n| print(n)));
    }

    pub fn par_inner(print: impl Fn(&u32) + Sync) {
        rayon::join(|| print(&0), || print(&0));
    }
}

mod arc_ghost {
    use ghostcell::GhostToken;
    use ghostcell::dlist_arc::*;
    use rayon::prelude::*;

    /// Create a dlist segment.
    pub fn init<'id>(list_size: u32, token: &mut GhostToken<'id>) -> NodePtr<'id, u32> {
        let list: NodePtr<u32> = Node::new(0);
        let mut tail = list.clone();

        (1..list_size).for_each(|i| {
            let node = Node::new(i);
            Node::insert_next(&tail, node.clone(), token);
            tail = node;
        });

        list
    }

    pub fn par<'id, T: Send + Sync>(
        num_threads: usize,
        list: &NodePtr<'id, T>,
        token: &GhostToken<'id>,
        f: impl Fn(&T) + Sync,
    ) {
        (0..num_threads)
            .into_par_iter()
            .for_each(|_| Node::iterate(list, token, |n| f(n)));
    }
}

mod arena_rwlock {
    use rayon::prelude::*;
    use std::{mem, sync::RwLock};
    use typed_arena::Arena as TypedArena;

    pub struct Node<'arena, T> {
        data: T,
        prev: Option<NodeRef<'arena, T>>,
        next: Option<NodeRef<'arena, T>>,
    }
    pub type NodeRef<'arena, T> = &'arena RwLock<Node<'arena, T>>;

    impl<'arena, T> Node<'arena, T> {
        pub fn new(
            value: T,
            arena: &'arena TypedArena<RwLock<Node<'arena, T>>>
        ) -> NodeRef<'arena, T> {
            arena.alloc(RwLock::new(
                Self {
                    data: value,
                    prev: None,
                    next: None,
                }
            ))
        }

        pub fn prev(&self) -> Option<NodeRef<'arena, T>> {
            self.prev
        }
    
        pub fn next(&self) -> Option<NodeRef<'arena, T>> {
            self.next
        }

        /// Unlink the nodes adjacent to `node`.
        pub fn remove(node: NodeRef<'arena, T>) {
            let node = node.write().unwrap();
            let old_prev: Option<NodeRef<_>> = node.prev;
            let old_next: Option<NodeRef<_>> = node.next;
            if let Some(old_next) = old_next {
                old_next.write().unwrap().prev = old_prev;
            }
            if let Some(old_prev) = old_prev {
                old_prev.write().unwrap().next = old_next;
            }
        }

        /// Insert `next` right after `node` in the list.
        pub fn insert_next(
            node: NodeRef<'arena, T>,
            next: NodeRef<'arena, T>
        ) {
            // Step 1: unlink the prev and next pointers nodes adjacent to next.
            Self::remove(next);

            // Step 2: link node.next pointer and old_node_next.prev pointer to next.
            let old_node_next: Option<NodeRef<T>> =
                mem::replace(&mut node.write().unwrap().next, Some(next));
            if let Some(old_node_next) = old_node_next {
                old_node_next.write().unwrap().prev = Some(next);
            }

            // Step 3: link next to node and old_node_next.
            let next: &mut Node<T> = &mut next.write().unwrap();
            next.prev = Some(node);
            next.next = old_node_next;
        }

        pub fn iterate(
            node: NodeRef<'arena, T>,
            f: impl Fn(&T),
        ) {
            let mut cur: Option<NodeRef<T>> = Some(node);
            while let Some(node) = cur {
                let node: &Node<T> = &node.read().unwrap();
                f(&node.data);
                cur = node.next;
            }
        }
    }
    /// Create a dlist segment.
    pub fn init<'arena>(
        arena: &'arena TypedArena<RwLock<Node<'arena, u32>>>,
        list_size: u32) -> NodeRef<'arena, u32> {
        let list: NodeRef<'arena, u32> = &*Node::new(0, &arena);
        let mut tail = list;

        (1..list_size).for_each(|i| {
            let node = Node::new(i, &arena);
            Node::insert_next(tail, &node);
            tail = node;
        });

        list
    }

    pub fn par<'arena, T: Send + Sync>(
        num_threads: usize,
        list: NodeRef<'arena, T>,
        f: impl Fn(&T) + Sync,
    ) {
        (0..num_threads)
            .into_par_iter()
            .for_each(|_| Node::iterate(list, |n| f(n)));
    }
}


mod arena_mutex {
    use rayon::prelude::*;
    use std::{mem, sync::Mutex};
    use typed_arena::Arena as TypedArena;

    pub struct Node<'arena, T> {
        data: T,
        prev: Option<NodeRef<'arena, T>>,
        next: Option<NodeRef<'arena, T>>,
    }
    pub type NodeRef<'arena, T> = &'arena Mutex<Node<'arena, T>>;

    impl<'arena, T> Node<'arena, T> {
        pub fn new(
            value: T,
            arena: &'arena TypedArena<Mutex<Node<'arena, T>>>
        ) -> NodeRef<'arena, T> {
            arena.alloc(Mutex::new(
                Self {
                    data: value,
                    prev: None,
                    next: None,
                }
            ))
        }

        pub fn prev(&self) -> Option<NodeRef<'arena, T>> {
            self.prev
        }
    
        pub fn next(&self) -> Option<NodeRef<'arena, T>> {
            self.next
        }

        /// Unlink the nodes adjacent to `node`.
        pub fn remove(node: NodeRef<'arena, T>) {
            let node = node.lock().unwrap();
            let old_prev: Option<NodeRef<_>> = node.prev;
            let old_next: Option<NodeRef<_>> = node.next;
            if let Some(old_next) = old_next {
                old_next.lock().unwrap().prev = old_prev;
            }
            if let Some(old_prev) = old_prev {
                old_prev.lock().unwrap().next = old_next;
            }
        }

        /// Insert `next` right after `node` in the list.
        pub fn insert_next(
            node: NodeRef<'arena, T>,
            next: NodeRef<'arena, T>
        ) {
            // Step 1: unlink the prev and next pointers nodes adjacent to next.
            Self::remove(next);

            // Step 2: link node.next pointer and old_node_next.prev pointer to next.
            let old_node_next: Option<NodeRef<T>> =
                mem::replace(&mut node.lock().unwrap().next, Some(next));
            if let Some(old_node_next) = old_node_next {
                old_node_next.lock().unwrap().prev = Some(next);
            }

            // Step 3: link next to node and old_node_next.
            let next: &mut Node<T> = &mut next.lock().unwrap();
            next.prev = Some(node);
            next.next = old_node_next;
        }

        pub fn iterate(
            node: NodeRef<'arena, T>,
            f: impl Fn(&T),
        ) {
            let mut cur: Option<NodeRef<T>> = Some(node);
            while let Some(node) = cur {
                let node: &Node<T> = &node.lock().unwrap();
                f(&node.data);
                cur = node.next;
            }
        }
    }
    /// Create a dlist segment.
    pub fn init<'arena>(
        arena: &'arena TypedArena<Mutex<Node<'arena, u32>>>,
        list_size: u32) -> NodeRef<'arena, u32> {
        let list: NodeRef<'arena, u32> = &*Node::new(0, &arena);
        let mut tail = list;

        (1..list_size).for_each(|i| {
            let node = Node::new(i, &arena);
            Node::insert_next(tail, &node);
            tail = node;
        });

        list
    }

    pub fn par<'arena, T: Send + Sync>(
        num_threads: usize,
        list: NodeRef<'arena, T>,
        f: impl Fn(&T) + Sync,
    ) {
        (0..num_threads)
            .into_par_iter()
            .for_each(|_| Node::iterate(list, |n| f(n)));
    }
}

mod stdlinkedlist {
    use rayon::prelude::*;
    use std::collections::LinkedList;

    pub fn init<'id>(list_size: u32) -> LinkedList<u32> {
        let mut list = LinkedList::new();

        (1..list_size).for_each(|i| {
            list.push_back(i);
        });

        list
    }

    pub fn par<T: Send + Sync>(num_threads: usize, list: &LinkedList<T>, f: impl Fn(&T) + Sync) {
        (0..num_threads)
            .into_par_iter()
            .for_each(|_| list.iter().for_each(|n| f(n)));
    }
}

use ghostcell::GhostToken;
use std::time::{Instant, Duration};

fn bench_dlist(c: &mut Criterion) {
    let list_size = 100_000;
    c.bench_function("rayon overhead", |b| {
        b.iter(|| {
            arena::par_inner(|n| {
                black_box(n);
            });
        })
    });

    c.bench_function("dlist arena atomic_cell init", |b| {
        b.iter_custom(|iters| {
            let mut elapsed = Duration::new(0,0);
            for _i in 0..iters {
                let start = Instant::now();
                let arena = TypedArena::with_capacity(black_box(list_size as usize));
                black_box(atomic_cell::init(&arena, black_box(list_size)));
                elapsed += start.elapsed();
            }
            elapsed
        });
    });
    c.bench_function("dlist arena atomic_cell init no_drop", |b| {
        b.iter_custom(|iters| {
            let mut elapsed = Duration::new(0,0);
            for _i in 0..iters {
                let start = Instant::now();
                let arena = TypedArena::with_capacity(black_box(list_size as usize));
                let mut r = black_box(atomic_cell::init(&arena, black_box(list_size)));
                black_box(&mut r);
                elapsed += start.elapsed();
                drop(r);
            }
            elapsed
        });
    });
    c.bench_function("dlist arena atomic_cell par (×4)", |b| {
        let arena = TypedArena::with_capacity(list_size as usize);
        let list = atomic_cell::init(&arena, list_size);
        b.iter(|| {
            black_box(atomic_cell::par(4, &list, |n| {
                black_box(n);
            }));
        });
    });

    c.bench_function("dlist arena ghostcell init", |b| {
        GhostToken::new(|mut token| {
            b.iter_custom(|iters| {
                let mut elapsed = Duration::new(0,0);
                for _i in 0..iters {
                    let start = Instant::now();
                    let arena = TypedArena::with_capacity(black_box(list_size as usize));
                    black_box(arena::init(&arena, &mut token, black_box(list_size)));
                    elapsed += start.elapsed();
                }
                elapsed
            });
        });
    });
    c.bench_function("dlist arena ghostcell init no_drop", |b| {
        GhostToken::new(|mut token| {
            b.iter_custom(|iters| {
                let mut elapsed = Duration::new(0,0);
                for _i in 0..iters {
                    let start = Instant::now();
                    let arena = TypedArena::with_capacity(black_box(list_size as usize));
                    let mut r = black_box(arena::init(&arena, &mut token, black_box(list_size)));
                    black_box(&mut r);
                    elapsed += start.elapsed();
                    drop(r);
                }
                elapsed
            });
        });
    });
    c.bench_function("dlist arena ghostcell par (×4)", |b| {
        GhostToken::new(|mut token| {
            let arena = TypedArena::with_capacity(list_size as usize);
            let list = arena::init(&arena, &mut token, list_size);
            b.iter(|| {
                black_box(arena::par(4, &list, &token, |n| {
                    black_box(n);
                }));
            });
        });
    });

    c.bench_function("dlist arena rwlock init", |b| {
        b.iter_custom(|iters| {
            let mut elapsed = Duration::new(0,0);
            for _i in 0..iters {
                let start = Instant::now();
                let arena = TypedArena::with_capacity(black_box(list_size as usize));
                black_box(arena_rwlock::init(&arena, black_box(list_size)));
                elapsed += start.elapsed();
            }
            elapsed
        });
    });
    c.bench_function("dlist arena rwlock init no_drop", |b| {
        b.iter_custom(|iters| {
            let mut elapsed = Duration::new(0,0);
            for _i in 0..iters {
                let start = Instant::now();
                let arena = TypedArena::with_capacity(black_box(list_size as usize));
                let mut r = black_box(arena_rwlock::init(&arena, black_box(list_size)));
                black_box(&mut r);
                elapsed += start.elapsed();
                drop(r);
            }
            elapsed
        });
    });
    c.bench_function("dlist arena rwlock par (×4)", |b| {
        let arena = TypedArena::with_capacity(list_size as usize);
        let list = arena_rwlock::init(&arena, list_size);
        b.iter(|| {
            black_box(arena_rwlock::par(4, &list, |n| {
                black_box(n);
            }));
        });
    });

    c.bench_function("dlist arena mutex init", |b| {
        b.iter_custom(|iters| {
            let mut elapsed = Duration::new(0,0);
            for _i in 0..iters {
                let start = Instant::now();
                let arena = TypedArena::with_capacity(black_box(list_size as usize));
                black_box(arena_mutex::init(&arena, black_box(list_size)));
                elapsed += start.elapsed();
            }
            elapsed
        });
    });
    c.bench_function("dlist arena mutex init no_drop", |b| {
        b.iter_custom(|iters| {
            let mut elapsed = Duration::new(0,0);
            for _i in 0..iters {
                let start = Instant::now();
                let arena = TypedArena::with_capacity(black_box(list_size as usize));
                let mut r = black_box(arena_mutex::init(&arena, black_box(list_size)));
                black_box(&mut r);
                elapsed += start.elapsed();
                drop(r);
            }
            elapsed
        });
    });
    c.bench_function("dlist arena mutex par (×4)", |b| {
        let arena = TypedArena::with_capacity(list_size as usize);
        let list = arena_mutex::init(&arena, list_size);
        b.iter(|| {
            black_box(arena_mutex::par(4, &list, |n| {
                black_box(n);
            }));
        });
    });

    c.bench_function("dlist arc ghostcell init", |b| {
        GhostToken::new(|mut token| {
            b.iter_custom(|iters| {
                let mut elapsed = Duration::new(0,0);
                for _i in 0..iters {
                    let start = Instant::now();
                    black_box(arc_ghost::init(black_box(list_size), &mut token));
                    elapsed += start.elapsed();
                }
                elapsed
            });
        });
    });
    c.bench_function("dlist arc ghostcell init no_drop", |b| {
        GhostToken::new(|mut token| {
            b.iter_custom(|iters| {
                let mut elapsed = Duration::new(0,0);
                for _i in 0..iters {
                    let start = Instant::now();
                    let mut r = black_box(arc_ghost::init(black_box(list_size), &mut token));
                    black_box(&mut r);
                    elapsed += start.elapsed();
                    drop(r);
                }
                elapsed
            });
        });
    });
    c.bench_function("dlist arc ghostcell par (×4)", |b| {
        GhostToken::new(|mut token| {
            let list = arc_ghost::init(list_size, &mut token);
            b.iter(|| {
                black_box(arc_ghost::par(4, &list, &token, |n| {
                    black_box(n);
                }));
            });
        });
    });

    c.bench_function("dlist stdLinkedList init", |b| {
        b.iter_custom(|iters| {
            let mut elapsed = Duration::new(0, 0);
            for _i in 0..iters {
                let start = Instant::now();
                black_box(stdlinkedlist::init(black_box(list_size)));
                elapsed += start.elapsed();
            }
            elapsed
        });
    });
    c.bench_function("dlist stdLinkedList init no_drop", |b| {
        b.iter_custom(|iters| {
            let mut elapsed = Duration::new(0, 0);
            for _i in 0..iters {
                let start = Instant::now();
                let mut r = black_box(stdlinkedlist::init(black_box(list_size)));
                black_box(&mut r);
                elapsed += start.elapsed();
                drop(r);
            }
            elapsed
        });
    });
    c.bench_function("dlist stdLinkedList par (×4)", |b| {
        let list = stdlinkedlist::init(list_size);
        b.iter(|| {
            black_box(stdlinkedlist::par(4, &list, |n| {
                black_box(n);
            }));
        });
    });
}

criterion_group!(benches, bench_dlist);
criterion_main!(benches);
