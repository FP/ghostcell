//! A doubly-linked list built with `GhostCell`. This variant uses `typed_arena::Arena` for memory management.

use crate::{GhostCell, GhostToken};
use typed_arena::Arena as TypedArena;

/// A doubly-linked list node.
pub struct Node<'arena, 'id, T> {
    data: T,
    prev: Option<NodeRef<'arena, 'id, T>>,
    next: Option<NodeRef<'arena, 'id, T>>,
}

/// A reference to a node.
pub type NodeRef<'arena, 'id, T> = &'arena GhostCell<'id, Node<'arena, 'id, T>>;

impl<'arena, 'id, T> Node<'arena, 'id, T> {
    /// Create a new isolated node from T. Requires an arena.
    pub fn new(
        data: T,
        arena: &'arena TypedArena<Node<'arena, 'id, T>>
    ) -> NodeRef<'arena, 'id, T> {
        GhostCell::from_mut(arena.alloc(
            Self {
                data,
                prev: None,
                next: None,
            }
        ))
    }

    pub fn prev(&self) -> Option<NodeRef<'arena, 'id, T>> {
        self.prev
    }

    pub fn next(&self) -> Option<NodeRef<'arena, 'id, T>> {
        self.next
    }

    /// Insert `node2` right after `node1` in the list.
    pub fn insert_next(
        node1: NodeRef<'arena, 'id, T>,
        node2: NodeRef<'arena, 'id, T>,
        token: &mut GhostToken<'id>,
    ) {
        // Step 1: unlink the prev and next pointers nodes adjacent to node2.
        Self::remove(node2, token);

        // Step 2: link node1.next pointer and node1.next.prev pointer to node2.
        let node1_old_next : Option<NodeRef<_>> = node1.borrow(token).next;
        if let Some(node1_old_next) = node1_old_next {
            node1_old_next.borrow_mut(token).prev = Some(node2);
        }
        node1.borrow_mut(token).next = Some(node2);

        // Step 3: link next to node and old_node_next (old node.next).
        let node2: &mut Node<_> = node2.borrow_mut(token);
        node2.prev = Some(node1);
        node2.next = node1_old_next;
    }

    /// Remove `node` to and from its adjacent nodes.
    pub fn remove(node: NodeRef<'arena, 'id, T>, token: &mut GhostToken<'id>) {
        // Step 1: unlink the prev and next pointers nodes adjacent to node.
        let old_prev = node.borrow(token).prev;
        let old_next = node.borrow(token).next;
        if let Some(next) = old_next {
            next.borrow_mut(token).prev = old_prev;
        }
        if let Some(prev) = old_prev {
            prev.borrow_mut(token).next = old_next;
        }

        // Step 2: null out the prev and next pointers.
        // let node = node.borrow_mut(token);
        // node.prev = None;
        // node.next = None;
    }

    /// Construct an imutable iterator to traverse immutably.
    pub fn iter<'iter>(
        node: NodeRef<'arena, 'id, T>,
        token: &'iter GhostToken<'id>,
    ) -> Iter<'arena, 'id, 'iter, T> {
        Iter {
            cur: Some(node),
            token,
        }
    }

    /// Mutable iteration only works as "interior iteration", since we cannot hand out mutable references
    /// to multiple nodes at the same time.
    pub fn iter_mut(
        node: NodeRef<'arena, 'id, T>,
        token: &mut GhostToken<'id>,
        mut f: impl FnMut(&mut T),
    ) {
        let mut cur: Option<NodeRef<'arena, 'id, T>> = Some(node);
        while let Some(node) = cur {
            let node = node.borrow_mut(token); // mutably borrow `node` with `token`
            f(&mut node.data);
            cur = node.next;
        }
    }

    /// Immutable interior iteration.
    pub fn iterate(
        node: NodeRef<'arena, 'id, T>,
        token: &GhostToken<'id>,
        f: impl Fn(&T),
    ) {
        let mut cur: Option<NodeRef<'arena, 'id, T>> = Some(node);
        while let Some(node) = cur {
            let node : &Node<_> = node.borrow(token); // immutably borrow `node` with `token`
            f(&node.data);
            cur = node.next;
        }
    }
}

/// An immutable iterator.
pub struct Iter<'arena, 'id, 'iter, T> {
    // We could probably weaken this to make the outermost reference a `'iter` (currenty it
    // implicitly is `'arena` since we use the `NodeRef` type), but that would make the types more
    // complicated, so we don't.
    cur: Option<NodeRef<'arena, 'id, T>>,
    token: &'iter GhostToken<'id>,
}

impl<'arena, 'id, 'iter, T> Iterator for Iter<'arena, 'id, 'iter, T>
where
    T: 'iter,
    'arena: 'iter,
{
    type Item = &'iter T;

    #[inline(always)]
    fn next(&mut self) -> Option<Self::Item> {
        let cur: Option<NodeRef<'arena, 'id, T>> = self.cur;
        if let Some(node) = cur {
            let node: NodeRef<'arena, 'id, T> = node;
            let node = node.borrow(self.token);
            self.cur = node.next;
            Some(&node.data)
        } else {
            None
        }
    }
}
