//! A singly-linked list built with `GhostCell`. This variant uses `typed_arena::Arena` for memory management.

use crate::{GhostCell, GhostToken};
use typed_arena::Arena as TypedArena;
/// A singly-linked list node.
pub struct Node<'arena, 'id, T> {
    data: T,
    next: Option<NodeRef<'arena, 'id, T>>,
}

/// A reference to a node.
pub type NodeRef<'arena, 'id, T> = &'arena GhostCell<'id, Node<'arena, 'id, T>>;

impl<'arena, 'id, T> Node<'arena, 'id, T> {
    /// Create a new isolated node from T. Requires an arena.
    pub fn new(
        data: T,
        arena: &'arena TypedArena<Node<'arena, 'id, T>>
    ) -> NodeRef<'arena, 'id, T> {
        GhostCell::from_mut(arena.alloc(
            Self {
                data,
                next: None,
            }
        ))
    }

    pub fn get_next(&self) -> Option<NodeRef<'arena, 'id, T>> {
        self.next
    }

    /// Set this node to point to child
    pub fn set_next(
        &mut self,
        child: NodeRef<'arena, 'id, T>
    ) {
        self.next = Some(child);
    }

    /// Construct an imutable iterator to traverse immutably.
    pub fn iter<'iter>(
        node: NodeRef<'arena, 'id, T>,
        token: &'iter GhostToken<'id>,
    ) -> Iter<'arena, 'id, 'iter, T> {
        Iter {
            cur: Some(node),
            token,
        }
    }
}

/// An immutable iterator.
pub struct Iter<'arena, 'id, 'iter, T> {
    // We could probably weaken this to make the outermost reference a `'iter`, but that
    // would make the types more complicated, so we don't.
    cur: Option<NodeRef<'arena, 'id, T>>,
    token: &'iter GhostToken<'id>,
}

impl<'arena, 'id, 'iter, T> Iterator for Iter<'arena, 'id, 'iter, T>
where
    T: 'iter,
    'arena: 'iter,
{
    type Item = &'iter T;

    #[inline(always)]
    fn next(&mut self) -> Option<Self::Item> {
        let cur: Option<NodeRef<'arena, 'id, T>> = self.cur;
        if let Some(node) = cur {
            let node: NodeRef<'arena, 'id, T> = node;
            let node = node.borrow(self.token);
            self.cur = node.next;
            Some(&node.data)
        } else {
            None
        }
    }
}
