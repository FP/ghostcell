//! A doubly-linked list built with `GhostCell`. This variant uses `sync::Arc` for memory management.

use crate::{GhostCell, GhostToken};
use std::sync::{Arc, Weak};

/// A doubly-linked list node.
pub struct Node<'id, T> {
    data: T,
    prev: Option<WeakNodePtr<'id, T>>,
    next: Option<NodePtr<'id, T>>,
}
/// A `Weak` pointer to a node.
pub type WeakNodePtr<'id, T> = Weak<GhostCell<'id, Node<'id, T>>>;
/// A strong `Arc` pointer to a node.
pub type NodePtr<'id, T> = Arc<GhostCell<'id, Node<'id, T>>>;

impl<'id, T> Node<'id, T> {
    pub fn new(value: T) -> NodePtr<'id, T> {
        Arc::new(GhostCell::new(
            Self {
                data: value,
                prev: None,
                next: None,
            }
        ))
    }

    pub fn prev_weak(&self) -> Option<&WeakNodePtr<'id, T>> {
        self.prev.as_ref()
    }

    pub fn prev(&self) -> Option<NodePtr<'id, T>> {
        self.prev_weak().and_then(|p| p.upgrade())
    }

    pub fn next(&self) -> Option<&NodePtr<'id, T>> {
        self.next.as_ref()
    }

    /// Unlink the nodes adjacent to `node`. The node will have `next` and `prev` be `None` after this.
    pub fn remove<'a>(node: &NodePtr<'id, T>, token: &'a mut GhostToken<'id>) {
        // `take` both pointers from `node`, setting its fields to `None`.
        let node = node.borrow_mut(token);
        let old_prev: Option<NodePtr<'id, T>> = node.prev.as_ref().and_then(|p| p.upgrade());
        let old_next: Option<NodePtr<'id, T>> = node.next.take();
        // link `old_prev` and `old_next together
        if let Some(old_next) = &old_next {
            old_next.borrow_mut(token).prev = old_prev.as_ref().map(|p| Arc::downgrade(&p));
        }
        if let Some(old_prev) = &old_prev {
            old_prev.borrow_mut(token).next = old_next;
        }
    }

    /// Insert `node2` right after `node1` in the list.
    pub fn insert_next<'a>(
        node1: &NodePtr<'id, T>,
        node2: NodePtr<'id, T>,
        token: &'a mut GhostToken<'id>,
    ) {
        // Step 1: unlink the prev and next pointers of nodes that are
        // adjacent to node2.
        Self::remove(&node2, token);

        // Step 2: get out the old next pointer as node1_old_next.
        let node1_old_next: Option<NodePtr<'id, T>> = node1.borrow_mut(token).next.take();
        if let Some(node1_old_next) = &node1_old_next {
            node1_old_next.borrow_mut(token).prev = Some(Arc::downgrade(&node2));
        }

        // Step 3: link node2 to node1 and node1_old_next.
        let node2_inner: &mut Node<'id, T> = node2.borrow_mut(token);
        node2_inner.prev = Some(Arc::downgrade(node1));
        node2_inner.next = node1_old_next;

        // Step 4: Link node1.next to node2.
        node1.borrow_mut(token).next = Some(node2);
    }

    /// Construct an imutable iterator to traverse immutably.
    pub fn iter<'iter>(
        node: &'iter NodePtr<'id, T>,
        token: &'iter GhostToken<'id>,
    ) -> Iter<'id, 'iter, T> {
        Iter {
            cur: Some(node.as_ref()),
            token,
        }
    }

    /// Mutable iteration only works as "interior iteration", since we cannot hand out mutable references
    /// to multiple nodes at the same time.
    pub fn iter_mut(
        node: &NodePtr<'id, T>,
        token: &mut GhostToken<'id>,
        mut f: impl FnMut(&mut T),
    ) {
        let mut cur: Option<NodePtr<'id, T>> = Some(node.clone());
        while let Some(node) = cur {
            let node: &mut Node<'id, T> = node.borrow_mut(token); // mutably borrow `node` with `token`
            f(&mut node.data);
            cur = node.next.clone();
        }
    }

    /// Immutable interior traversal.
    pub fn iterate(
        node: &NodePtr<'id, T>,
        token: &GhostToken<'id>,
        f: impl Fn(&T),
    ) {
        let mut cur: Option<&GhostCell<'id, Node<'id, T>>> = Some(node.as_ref());
        while let Some(node) = cur {
            let node: &Node<'id, T> = node.borrow(token); // immutably borrow `node` with `token`
            f(&node.data);
            cur = node.next.as_deref();
        }
    }
}

/// An immutable iterator.
pub struct Iter<'id, 'iter, T> {
    cur: Option<&'iter GhostCell<'id, Node<'id, T>>>,
    token: &'iter GhostToken<'id>,
}

impl<'id, 'iter, T> Iterator for Iter<'id, 'iter, T>
where
    T: 'iter,
{
    type Item = &'iter T;

    #[inline(always)]
    fn next(&mut self) -> Option<Self::Item> {
        if let Some(node) = self.cur {
            let node: &Node<'id, T> = node.borrow(self.token); // immutably borrow `node` with `token`
            self.cur = node.next.as_deref();
            Some(&node.data)
        } else {
            None
        }
    }
}
