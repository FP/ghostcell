//! A simple DFS built with `GhostCell`. This variant uses `typed_arena::Arena` for memory management.

use crate::{GhostCell, GhostToken};
use typed_arena::Arena as TypedArena;
use fixedbitset::FixedBitSet;

/// A graph node.
pub struct Node<'arena, 'id, T> {
    data : T,
    uid : u32,
    edges : Vec<NodeRef<'arena, 'id, T>>,
}

/// A reference to a node.
pub type NodeRef<'arena, 'id, T> = &'arena GhostCell<'id, Node<'arena, 'id, T>>;

impl<'arena, 'id, T> Node<'arena, 'id, T> {
    /// Create a new isolated node from T. Requires an arena.
    fn new(
        data : T,
        uid : u32,
        arena : &'arena TypedArena<Node<'arena, 'id, T>>
    ) -> NodeRef<'arena, 'id, T> {
        GhostCell::from_mut(arena.alloc(
            Self {
                data: data,
                uid: uid,
                edges: vec![],
            }
        ))
    }

    fn with_capacity(
        data: T,
        uid : u32,
        edge_size : usize,
        arena : &'arena TypedArena<Node<'arena, 'id, T>>
    ) -> NodeRef<'arena, 'id, T> {
        GhostCell::from_mut(arena.alloc(
            Self {
                data: data,
                uid : uid,
                edges: Vec::with_capacity(edge_size),
            }
        ))
    }

    pub fn add_edge(&mut self, child : NodeRef<'arena, 'id, T>) {
        self.edges.push(child)
    }

    pub fn uid(&self) -> u32 {
        self.uid
    }
}

pub struct Graph<'arena, 'id, T> {
    nodes : Vec<NodeRef<'arena, 'id, T>>,
}

impl<'arena, 'id, T> Graph<'arena, 'id, T> {
    pub fn new(node_size : usize) -> Self {
        Self {
            nodes: Vec::with_capacity(node_size),
        }
    }

    pub fn add_node(
        &mut self,
        data : T,
        arena : &'arena TypedArena<Node<'arena, 'id, T>>
    ) -> NodeRef<'arena, 'id, T> {
        let node = Node::new(data, self.nodes.len() as u32, arena);
        self.nodes.push(node);
        node
    }

    pub fn add_node_with_capacity(
        &mut self,
        data: T,
        edge_size : usize,
        arena : &'arena TypedArena<Node<'arena, 'id, T>>
    ) -> NodeRef<'arena, 'id, T> {
        let node = Node::with_capacity(data, self.nodes.len() as u32, edge_size, arena);
        self.nodes.push(node);
        node
    }

    pub fn nodes_count(&self) -> usize {
        self.nodes.len()
    }

    pub fn nodes(&self) -> &Vec<NodeRef<'arena, 'id, T>> {
        &self.nodes
    }

    pub fn dfs_visitor(
        &self,
        root: NodeRef<'arena, 'id, T>
    ) -> DFSVisitor<'arena, 'id, T> {
        DFSVisitor {
            stack: vec![root],
            mark: FixedBitSet::with_capacity(self.nodes.len()),
        }
    }
}

/// A DFS traversal structure that stores a visit map.
pub struct DFSVisitor<'arena, 'id, T>{
    stack: Vec<NodeRef<'arena, 'id, T>>,
    mark: FixedBitSet,
}

impl<'arena, 'id, T> DFSVisitor<'arena, 'id, T> {
    pub fn reset(
        &mut self,
        graph: &Graph<'arena, 'id, T>,
        root: NodeRef<'arena, 'id, T>
    ) {
        self.stack.clear();
        self.stack.push(root);
        self.mark.clear();
        self.mark.grow(graph.nodes_count());
    }

    pub fn iter<'iter>(
        &'iter mut self,
        token: &'iter GhostToken<'id>
    ) -> DFSIter<'arena, 'id, 'iter, T> {
        DFSIter {
            _inner: self,
            token: token
        }
    }

    fn visit(&mut self, n: u32) {
        self.mark.put(n as usize);
    }

    fn is_visited(&self, n: u32) -> bool {
        self.mark.contains(n as usize)
    }

    pub fn iter_mut(
        &mut self,
        token : &mut GhostToken<'id>,
        mut f: impl FnMut(&mut T),
    ) {
        while let Some(node) = self.stack.pop() {
            let node_mut: &mut Node<_> = node.borrow_mut(token);
            let uid = node_mut.uid;
            if !self.is_visited(uid) {
                self.visit(uid);
                f(&mut node_mut.data);
                let node: &Node<_> = node.borrow(token);
                for child in node.edges.iter() {
                    if !self.is_visited(child.borrow(token).uid) {
                        self.stack.push(child)
                    }
                }
            }
        }
    }
}

pub struct DFSIter<'arena, 'id, 'iter, T> {
    _inner: &'iter mut DFSVisitor<'arena, 'id, T>,
    token: &'iter GhostToken<'id>,
}

impl<'arena, 'id, 'iter, T> Iterator for DFSIter<'arena, 'id, 'iter, T>
where
    T: 'iter,
    'arena: 'iter,
{
    type Item = &'iter T;

    #[inline(always)]
    fn next(&mut self) -> Option<Self::Item> {
        while let Some(node) = self._inner.stack.pop() {
            let node = node.borrow(self.token);
            let uid = node.uid;
            if !self._inner.is_visited(uid) {
                self._inner.visit(uid);
                for child in node.edges.iter() {
                    if !self._inner.is_visited(child.borrow(self.token).uid) {
                        self._inner.stack.push(child)
                    }
                }
                return Some(&node.data)
            }
        }
        None
    }
}
