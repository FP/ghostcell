extern crate petgraph;
use petgraph::graph::Graph;
use petgraph::graph::NodeIndex;
use petgraph::algo::*;
use petgraph::visit::Dfs;

pub fn init(node_size : usize) -> (Graph<u32, ()>, NodeIndex) {
    let edge_size = 4;
    let mut graph : Graph<u32, ()> = Graph::with_capacity(node_size, node_size * edge_size);
    let mut nodes : Vec<NodeIndex> = Vec::with_capacity(node_size);

    for i in 0..node_size as u32 {
        nodes.push(graph.add_node(i))
    }
    // For the first half of the nodes, link every node to the next (edge_size - 1) nodes
    for i in 0..(node_size/2) {
        for j in 1..edge_size {
            graph.add_edge(nodes[i], nodes[i+j], ());
        }
    }
    // For the second half of the nodes, link every node to the previous (edge_size - 1) nodes
    for i in (node_size/2)..(node_size) {
        for j in 1..edge_size {
            graph.add_edge(nodes[i], nodes[i-j], ());
        }
    }

    // Also link the first half to the second half
    for i in 0..(node_size/2) {
        graph.add_edge(nodes[i], nodes[i+node_size/2], ());
    }
    for i in (node_size/2+1)..(node_size) {
        graph.add_edge(nodes[i], nodes[i-node_size/2], ());
    }

    (graph, nodes[0])
}

fn main() {
    let node_size = 100;
    let (mut graph, root) = init(node_size);

    println!("Numbers of nodes: {:?}", graph.node_count());
    println!("Numbers of edges: {:?}", graph.edge_count());
    println!("Is cyclic: {:?}", is_cyclic_directed(&graph));

    let mut dfs = Dfs::new(&graph, root);
    let mut sum_node_weights = 0;

    print!("[{}] ", root.index());
    while let Some(visited) = dfs.next(&graph) {
        sum_node_weights += graph[visited];
        print!(" {} ({})", visited.index(), graph[visited]);
        graph[visited] *= 10;
    }
    println!(); println!("Sum of node weights: {}", sum_node_weights);

    dfs.reset(&graph);
    dfs.move_to(root);
    sum_node_weights = 0;

    print!("[{}] ", root.index());
    while let Some(visited) = dfs.next(&graph) {
        sum_node_weights += graph[visited];
        print!(" {} ({})", visited.index(), graph[visited]);
        graph[visited] *= 10;
    }
    println!(); println!("Sum of node weights: {}", sum_node_weights);
}
