use ghostcell::dlist_arena::*;
use typed_arena::Arena as TypedArena;
use ghostcell::GhostToken;
use std::sync::RwLock;
use std::fmt;

fn print_list<'arena, 'id, T: fmt::Debug>(tag: char, list: NodeRef<'arena, 'id, T>, token: &GhostToken<'id>) {
    for d in Node::iter(list, token) {
        print!("{}{:?}, ", tag, d);
    }
}

fn init_list<'arena, 'id>(
    arena: &'arena TypedArena<Node<'arena, 'id, u32>>,
    token: &mut GhostToken<'id>,
    list_size: u32
) -> NodeRef<'arena, 'id, u32> {
    let head: NodeRef<_> = Node::new(0, arena);
    let mut tail = head;

    // To append to the list, we need a &mut GhostToken
    for i in 1..list_size {
        let node = Node::new(i, arena);
        Node::insert_next(tail, node, token);
        tail = node;
    }

    head
}

fn main() {
    GhostToken::new(|mut token| {
        // Allocate a list of size 50.
        let arena = TypedArena::with_capacity(50);
        let list = init_list(&arena, &mut token, 50);

        rayon::join(
            || Node::iterate(&list, &token, |n| print!("{:?}, ", n)),
            || Node::iterate(&list, &token, |n| print!("{:?}, ", n)),
        );

        // We can put a RwLock on the token to allow concurrent writes and reads.
        let lock_token: RwLock<GhostToken> = RwLock::new(token);
        // The read may come in before or after the write.
        // If the read comes in later, it should print the updated data.
        rayon::join(
            // fork two child threads
            || {
                let token: &GhostToken = &lock_token.read().unwrap(); // acquire read lock
                Node::iterate(&list, token, |n| print!("{:?}, ", n));
            },
            || {
                let token: &mut GhostToken = &mut lock_token.write().unwrap(); // acquire write lock
                Node::iter_mut(&list, token, |n| *n += 100); // add 100 to the nodes' data
            },
        );
    });
}
