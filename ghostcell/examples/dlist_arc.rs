use ghostcell::dlist_arc::*;
use ghostcell::GhostToken;
use std::sync::RwLock;
use std::{fmt, thread, time};

fn print_list<'id, T: fmt::Debug>(tag: char, list: &NodePtr<'id, T>, token: &GhostToken<'id>) {
    for d in Node::iter(list, token) {
        print!("{}{:?}, ", tag, d);
    }
}

fn init_list<'id>(token: &mut GhostToken<'id>, list_size: u32) -> (NodePtr<'id, u32>, NodePtr<'id, u32>) {
    let head: NodePtr<u32> = Node::new(0);
    let mut tail = head.clone();

    // To append to the list, we need a &mut GhostToken
    for i in 1..list_size {
        let node = Node::new(i);
        Node::insert_next(&tail, node.clone(), token);
        tail = node;
    };

    (head, tail)
}

fn main() {
    GhostToken::new(|mut token| {
        let list_size = 50;

        // Allocate a list from 0 to list_size - 1
        let (list, tail) = init_list(&mut token, list_size);

        // Print the list we created
        print!("Numbers: ");
        // This only needs a &GhostToken
        print_list(' ', &list, &token);
        println!();

        // Oh, let's print it in parallel with thread `a` and thread `b`.
        // Both threads only need a shared reference &token
        // You should try rerunning several times to see that the `a`'s and
        // `b`'s can interleave.
        println!("Parallel printing: ");
        rayon::join(
            ||  print_list('a', &list, &token),
            ||  print_list('b', &list, &token),
        );
        println!();

        // Upon rejoining, we can mutate the list again.
        // Delete the second half of the list, by deleting what tail points to.
        // Mutation requires &mut GhostToken
        (1..list_size / 2).for_each(|_| {
            if let Some(prev) = tail.borrow(&token).prev() {
                Node::remove(&prev, &mut token)
            };
        });

        print!("Post deletion: ");
        print_list(' ', &list, &token);
        println!();

        // RwLock on the token to allow concurrent writes and reads.
        let token : RwLock<GhostToken> = RwLock::new(token);

        // Let's do concurrent reads first.
        // You should try rerunning several times to see that the `a`'s and
        // `b`'s can interleave.
        println!("RwLock Concurrent Reads: ");
        rayon::join(
            || {
                let token : &GhostToken = &token.read().unwrap();
                print_list('a', &list, token);
            },
            || {
                let token : &GhostToken = &token.read().unwrap();
                print_list('b', &list, token);
            }
        );
        println!();

        println!("RwLock Concurrent Reads and Writes: ");
        // You should rerun this several times to see that the read may come in
        // before or after the write. If the read comes in later, it should
        // print the updated data.
        rayon::join(
            || {
                // Sleeps to allow the writer thread to acquire a writer lock.
                thread::sleep(time::Duration::from_micros(10));
                let token : &GhostToken = &token.read().unwrap();
                print_list('r', &list, token);
            },
            || {
                let token : &mut GhostToken = &mut token.write().unwrap();
                Node::iter_mut(&list, token, |n| {
                    // add a 100 to the nodes' data
                    *n += 100; print!("w{:?} ", n);
                });
            },
        );
        println!();
    });
}
