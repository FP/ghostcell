use typed_arena::Arena as TypedArena;
use std::sync::RwLock;
use fixedbitset::FixedBitSet;

/// A graph node.
struct NodeBase<'arena, T> {
    data : T,
    edges : Vec<NodeRef<'arena, T>>,
}

pub struct Node<'arena, T> {
    uid : u32,
    inner : RwLock<NodeBase<'arena, T>>
}

/// A reference to a node.
pub type NodeRef<'arena, T> = &'arena Node<'arena, T>;

impl<'arena, T> NodeBase<'arena, T> {
    pub fn add_edge(&mut self, child : NodeRef<'arena, T>) {
        self.edges.push(child)
    }
}

impl<'arena, T> Node<'arena, T> {
    /// Create a new isolated node from T. Requires an arena.
    fn new(
        data : T,
        uid : u32,
        arena : &'arena TypedArena<Node<'arena, T>>
    ) -> NodeRef<'arena, T> {
        arena.alloc(
            Self {
                uid: uid,
                inner: RwLock::new(
                    NodeBase {
                        data: data,
                        edges: vec![]
                    }),
            }
        )
    }

    fn with_capacity(
        data: T,
        uid : u32,
        edge_size : usize,
        arena : &'arena TypedArena<Node<'arena, T>>
    ) -> NodeRef<'arena, T> {
        arena.alloc(
            Self {
                uid: uid,
                inner: RwLock::new(
                    NodeBase {
                        data: data,
                        edges: Vec::with_capacity(edge_size),
                    }),
            }
        )
    }

    pub fn uid(&self) -> u32 {
        self.uid
    }
}

/// A graph with a root. Internally has a vector to keep track of all nodes in the graph.
pub struct Graph<'arena, T> {
    nodes : Vec<NodeRef<'arena, T>>,
}

impl<'arena, T> Graph<'arena, T> {
    pub fn new(node_size : usize) -> Self {
        Self {
            nodes: Vec::with_capacity(node_size),
        }
    }

    pub fn add_node(
        &mut self,
        data : T,
        arena : &'arena TypedArena<Node<'arena, T>>
    ) -> NodeRef<'arena, T> {
        let node = Node::new(data, self.nodes.len() as u32, arena);
        self.nodes.push(node);
        node
    }

    pub fn add_node_with_capacity(
        &mut self,
        data: T,
        edge_size : usize,
        arena : &'arena TypedArena<Node<'arena, T>>
    ) -> NodeRef<'arena, T> {
        let node = Node::with_capacity(data, self.nodes.len() as u32, edge_size, arena);
        self.nodes.push(node);
        node
    }

    pub fn nodes_count(&self) -> usize {
        self.nodes.len()
    }

    pub fn nodes(&self) -> &Vec<NodeRef<'arena, T>> {
        &self.nodes
    }

    pub fn dfs_iter(
        & self,
        root: NodeRef<'arena, T>
    ) -> DFSIter<'arena, T> {
        let mut stack = Vec::with_capacity(self.nodes.len()/2);
        stack.push(root);
        DFSIter {
            stack: stack,
            mark: FixedBitSet::with_capacity(self.nodes.len()),
        }
    }
}

/// A DFS traversal structure that stores a visit map.
pub struct DFSIter<'arena, T>{
    stack: Vec<NodeRef<'arena, T>>,
    mark: FixedBitSet,
}

impl<'arena, T> DFSIter<'arena, T> {
    pub fn reset(
        &mut self,
        graph: &Graph<'arena, T>,
        root: NodeRef<'arena, T>
    ) {
        self.stack.clear();
        self.stack.push(root);
        self.mark.clear();
        self.mark.grow(graph.nodes_count());
    }

    fn visit(&mut self, n: u32) -> bool {
        self.mark.put(n as usize)
    }

    fn is_visited(&self, n: u32) -> bool {
        self.mark.contains(n as usize)
    }

    pub fn iter(
        &mut self,
        f: impl Fn(&T),
    ) {
        while let Some(node) = self.stack.pop() {
            let uid = node.uid;
            let node = node.inner.read().unwrap();
            if !self.is_visited(uid) {
                self.visit(uid);
                f(&node.data);
                for child in node.edges.iter() {
                    if !self.is_visited(child.uid) {
                        self.stack.push(child)
                    }
                }
            }
        }
    }

    pub fn iter_mut(
        &mut self,
        mut f: impl FnMut(&mut T),
    ) {
        while let Some(node) = self.stack.pop() {
            let uid = node.uid;
            let mut node = node.inner.write().unwrap();
            if !self.is_visited(uid) {
                self.visit(uid);
                f(&mut node.data);
                for child in node.edges.iter() {
                    if !self.is_visited(child.uid) {
                        self.stack.push(child)
                    }
                }
            }
        }
    }
}

pub fn init<'arena>(
    arena : &'arena TypedArena<Node<'arena, u32>>,
    node_size : usize,
) -> (NodeRef<'arena, u32>, Graph<'arena, u32>) {
    let mut graph = Graph::new(node_size);
    let edge_size = 4;
    for i in 0..node_size as u32 {
        graph.add_node_with_capacity(i, edge_size, arena);
    }

    let nodes = graph.nodes();
    // For the first half of the nodes, link every node to the next (edge_size - 1) nodes
    for i in 0..(node_size/2) {
        for j in 1..edge_size {
            nodes[i].inner.write().unwrap().add_edge(nodes[i+j]);
        }
    }
    // For the second half of the nodes, link every node to the previous (edge_size - 1) nodes
    for i in (node_size/2)..(node_size) {
        for j in 1..edge_size {
            nodes[i].inner.write().unwrap().add_edge(nodes[i-j]);
        }
    }

    // Also link the first half to the second half
    for i in 0..(node_size/2) {
        nodes[i].inner.write().unwrap().add_edge(nodes[i+node_size/2]);
    }
    for i in (node_size/2+1)..(node_size) {
        nodes[i].inner.write().unwrap().add_edge(nodes[i-node_size/2]);
    }
    (nodes[0], graph)
}

fn main() {
    let node_size = 10;

    let arena = TypedArena::with_capacity(node_size as usize);
    let (root, graph) = init(&arena, node_size);
    let mut dfs = graph.dfs_iter(root);
    dfs.iter(|n| { print!(" {}", n) });
    println!();
    dfs.reset(&graph, root);
    dfs.iter_mut(|n| { *n *= 10 });
    dfs.reset(&graph, root);
    dfs.iter(|n| { print!(" {}", n) });
    println!();
}
