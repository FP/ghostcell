use ghostcell::GhostToken;
use ghostcell::dfs_arena_list::*;
use ghostcell::list_arena::Node as SNode;
use typed_arena::Arena as TypedArena;

pub fn init<'arena, 'id>(
    arena : &'arena TypedArena<Node<'arena, 'id, u32>>,
    larena : &'arena TypedArena<SNode<'arena, 'id, NodeRef<'arena, 'id, u32>>>,
    token : &mut GhostToken<'id>,
    node_size : usize,
) -> (NodeRef<'arena, 'id, u32>, Graph<'arena, 'id, u32>) {
    let mut graph = Graph::new(node_size);
    for i in 0..node_size as u32 {
        graph.add_node(i, arena);
    }

    let nodes = graph.nodes();
    // For the first half of the nodes, link every node to all later nodes
    for i in 1..(node_size/2) {
        for j in 0..i {
            Node::add_edge(nodes[j], nodes[i], token, larena);
        }
    }

    // Also link the first half to the second half
    for i in 0..(node_size/2) {
        Node::add_edge(nodes[i], nodes[i+node_size/2], token, larena);
        // nodes[i+node_size/2].borrow_mut(token).add_edge(nodes[i]);
    }
    for i in 0..(node_size/2-1) {
        Node::add_edge(nodes[i+node_size/2], nodes[i+1], token, larena);
    }

    (nodes[0], graph)
}

fn main() {
    let node_size = 10;
    let per_node_edge_size = 5;
    GhostToken::new(|mut token| {
        let arena = TypedArena::with_capacity(node_size as usize);
        let larena = TypedArena::with_capacity(node_size * per_node_edge_size as usize);
        let (root, graph) = init(&arena, &larena, &mut token, node_size);

        let mut visitor = graph.dfs_visitor(root);
        for n in visitor.iter(&token) {
            print!(" {}", n);
        }
        println!();

        visitor.reset(&graph, root);
        visitor.iter_mut(&mut token, |n| { *n *= 10 });

        visitor.reset(&graph, root);
        for n in visitor.iter(&token) {
            print!(" {}", n);
        }
        println!();
    });
}
