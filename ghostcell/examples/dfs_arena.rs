use ghostcell::GhostToken;
use ghostcell::dfs_arena::*;
use typed_arena::Arena as TypedArena;
use rayon::prelude::*;

pub fn init<'arena, 'id>(
    arena : &'arena TypedArena<Node<'arena, 'id, u32>>,
    token : &mut GhostToken<'id>,
    node_size : usize,
    per_node_edge_size : usize,
) -> (NodeRef<'arena, 'id, u32>, Graph<'arena, 'id, u32>) {
    let mut graph = Graph::new(node_size);
    for i in 0..node_size as u32 {
        graph.add_node_with_capacity(i, per_node_edge_size, arena);
    }

    let nodes = graph.nodes();
    // For the first half of the nodes, link every node to all later nodes
    for i in 1..(node_size/2) {
        for j in 0..i {
            nodes[j].borrow_mut(token).add_edge(nodes[i]);
        }
    }

    // Also link the first half to the second half
    for i in 0..(node_size/2) {
        nodes[i].borrow_mut(token).add_edge(nodes[i+node_size/2]);
        // nodes[i+node_size/2].borrow_mut(token).add_edge(nodes[i]);
    }
    for i in 0..(node_size/2-1) {
        nodes[i+node_size/2].borrow_mut(token).add_edge(nodes[i+1]);
    }

    (nodes[0], graph)
}

pub fn par<'arena, 'id>(
    num_threads: usize,
    root: NodeRef<'arena, 'id, u32>,
    graph: &Graph<'arena, 'id, u32>,
    token: &GhostToken<'id>,
    f: impl Fn(&u32) + Sync,
) {
    let mut visitors : Vec<_> =
        (0..num_threads).map(|_| graph.dfs_visitor(root)).collect();

    visitors.par_iter_mut()
            .for_each(|dfs| for n in dfs.iter(token) {
                f(n);
            });
    // let mut visitor = graph.dfs_visitor(root);
    // (0..num_threads)
    //     .into_par_iter()
    //     .for_each(|_| dfs_immut(root, graph, token, |n| { f(n) }));
}

fn main() {
    let node_size = 10;
    let per_node_edge_size = 5;
    GhostToken::new(|mut token| {
        let arena = TypedArena::with_capacity(node_size as usize);
        let (root, graph) = init(&arena, &mut token, node_size, per_node_edge_size);

        par(4, root, &graph, &token, |n| print!(" {}", n) );

        // let mut visitor = graph.dfs_visitor(root);
        // for n in visitor.iter(&token) {
        //     print!(" {}", n);
        // }
        // println!();

        // visitor.reset(&graph, root);
        // visitor.iter_mut(&mut token, |n| { *n *= 10 });

        // visitor.reset(&graph, root);
        // for n in visitor.iter(&token) {
        //     print!(" {}", n);
        // }
        // println!();
    });
}
